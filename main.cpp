#include <Arduino.h>
#include <TimerOne.h>
#include <LiquidCrystal_I2C.h>
#include <HX711.h>

const int dout = 13;
const int  clk = 12;
 
const int dirPin = 8;
const int stepPin = 9;

const int upBoton = 7;
const int stopBoton = 6;
const int downBoton = 5;

const int finSubida =2;



HX711 scale(dout, clk);
LiquidCrystal_I2C lcd(0x27,20,4);

//Configuracion de ensayo
//=============
bool direccion=LOW;  // LOW -> sube
bool ensayando=false;
int velocidad=0;  // mm/min


//variables internas
int potenciometro;
float calibration_factor = 45982; 
int count=0;  //debe poder guardar numeros grandes >100000
long posicion=0;
long stepDelay=100;
bool step_state=HIGH;
long periodo_muestreo=100000;
float fuerza=0;
bool bloqueo_subida=false;
bool bloqueo_bajada=false;

/* debe transformar la velocidad deseada a microsegundos
 * de tiempo de espera entre pulsos
 */
long vel2delay(int vel){
  float velo = (float)vel;
  long delay = 1000000/((velo/60)*2560);
  return delay;
}

void mostrarVelocidad(){
  /*
  Serial.print("Velocidad = ");
  Serial.print(velocidad);
  Serial.println(" mm/min"); */
  lcd.setCursor(3,1);
  lcd.print("         ");
  lcd.setCursor(3,1);
  lcd.print(velocidad);
}

void tomaDatos(){
  /* Funcion llamada para actualizar los datos de fuerza
  y desplazamiento, tambien actualiza pantalla y envia valor
  por el puerto serial*/
  fuerza=scale.get_units();
  // fuerza y d al serial
  if (ensayando){
  Serial.print(fuerza,2);
  Serial.print(", ");
  Serial.print((float)(posicion/2560.0),3);
  Serial.println();}
 //  pantalla lcd
  lcd.setCursor(3,2);
  lcd.print("         ");
  lcd.setCursor(3,2);
  lcd.print(fuerza,3);
  lcd.setCursor(3,3);
  lcd.print("         ");
  lcd.setCursor(3,3);
  lcd.print((float)(posicion/2560.0),3);
}

/* Funcion llamada cada vez que se alcanza el tiempo
 * de interrupts, cambia el estado de los pines de pulso
 * de motores.
 */
void moverMotores(){
  step_state=!step_state;
  digitalWrite(stepPin, step_state);
  count++;
  if (direccion==LOW){
    posicion++;
  }else if (direccion==HIGH){
    posicion--;
  }
}

void ensayo(){
  /*seteamos los microsegundos de acuerdo a la velocidad deseada
   * seteamos la direccion y finalmente entra en un loop 
   * que toma datos segun el tiempo indicado en periodo_muestreo
   * finalmente se interrumpe cuando se pulsa el boton de parada
   */
  stepDelay = vel2delay(velocidad);
  digitalWrite(dirPin, direccion);

  ensayando = true;
  Serial.println("ensayando");
  Timer1.attachInterrupt(moverMotores,stepDelay);
  // a partir de aqui cada stepDelay microsegundos 
  // se interrumpe todo y se cambia el pulso de los motores
  while (ensayando){
    if (count*2*stepDelay>periodo_muestreo){
      tomaDatos();
      count=0;
      if ((fuerza<-100.0)&&(direccion==true)){
         bloqueo_bajada=true;
        Timer1.detachInterrupt();  //se acaban las interrupciones
        ensayando = false;
        lcd.setCursor(9,0);
        lcd.print("          ");
        lcd.setCursor(9,0);
        lcd.print("[F<-lim]");
        Serial.println("fin_ensayo");
      }else if((fuerza>100.0)&&(direccion==false)){
         bloqueo_subida=true;
        Timer1.detachInterrupt();  //se acaban las interrupciones
        ensayando = false;
        lcd.setCursor(9,0);
        lcd.print("          ");
        lcd.setCursor(9,0);
        lcd.print("[F>lim]");
        Serial.println("fin_ensayo");
    }}
    if ((digitalRead(finSubida)==LOW)&&(direccion==LOW)){
        Timer1.detachInterrupt();  //se acaban las interrupciones
        bloqueo_subida=true;
        ensayando = false;
        lcd.setCursor(9,0);
        lcd.print("          ");
        lcd.setCursor(9,0);
        lcd.print("[lim_sup]");
        Serial.println("fin_ensayo");
      }
    
    if (digitalRead(stopBoton)==LOW){
      Timer1.detachInterrupt();  //se acaban las interrupciones
      ensayando = false;
      lcd.setCursor(9,0);
      lcd.print("          ");
      lcd.setCursor(9,0);
      lcd.print("[Detenido]");
      Serial.println("fin_ensayo");
    }
  }
}


void leerBotonesEnsayo(){
  if ((digitalRead(upBoton)==LOW)&&!bloqueo_subida){
    direccion = LOW;
    lcd.setCursor(9,0);
    lcd.print("          ");
    lcd.setCursor(9,0);
    lcd.print("[Trac]");
    ensayo();
  }else if ((digitalRead(downBoton)==LOW)&&!bloqueo_bajada){
    direccion = HIGH;
    lcd.setCursor(9,0);
    lcd.print("          ");
    lcd.setCursor(9,0);
    lcd.print("[Comp]");
    ensayo();
  }
}

void update_data(){
  tomaDatos();
  mostrarVelocidad();
}

void setup() {
  Serial.begin(9600);
  Serial.println("UniTester");
  scale.set_scale(calibration_factor);
  scale.tare();//Tara la celda de carga
  lcd.init(); 
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("UniTester");
  lcd.setCursor(9,0);
  lcd.print("[Detenido]");
  lcd.setCursor(0,1);
  lcd.print("V=");
  lcd.setCursor(12,1);
  lcd.print("mm/min");
  lcd.setCursor(0,2);
  lcd.print("F=");
  lcd.setCursor(12,2);
  lcd.print("Kgf");
  lcd.setCursor(0,3);
  lcd.print("d=");
  lcd.setCursor(12,3);
  lcd.print("mm");
  
  

  // Pines para controlar motores
  // ============================
  pinMode(dirPin, OUTPUT);
  pinMode(stepPin, OUTPUT);


  // Pines de botones
  // ================
  pinMode(upBoton, INPUT_PULLUP);
  pinMode(stopBoton, INPUT_PULLUP);
  pinMode(downBoton, INPUT_PULLUP);
  digitalWrite(dirPin, direccion);
  pinMode(finSubida, INPUT_PULLUP);
  
  Timer1.initialize(1000000);
}
  int cuenta_loop=0;
void loop() {
  cuenta_loop++;
  potenciometro=analogRead(A1); //valor de velocidad de ensayo
  if (potenciometro<512){
    velocidad = map(potenciometro, 0, 512, 1, 50);
  }else{
    velocidad = map(potenciometro, 512, 1023, 51, 1000);
  }
  
  leerBotonesEnsayo(); //revisar si hay que comenzar un ensayo
  if (cuenta_loop>20){
    update_data();  //actualizar datos no tan seguido para no perder pulsos de botones
  }
  if ((digitalRead(finSubida)==HIGH)&&(fuerza<100.0)&&bloqueo_subida){
    bloqueo_subida=false;
    lcd.setCursor(9,0);
    lcd.print("          ");
    lcd.setCursor(9,0);
    lcd.print("[Detenido]");
  }else if((fuerza>-100.0) && bloqueo_bajada){
    bloqueo_bajada=false;
    lcd.setCursor(9,0);
    lcd.print("          ");
    lcd.setCursor(9,0);
    lcd.print("[Detenido]");
  }
}
