import serial
import numpy as np
import re
import matplotlib.pyplot as plt
import sys
import glob




def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def capture():
    F=[]
    d=[]
    print("inicia captura")
    while True:
        datos_en_byte = ser.readline().strip()
        datos_en_string = datos_en_byte.decode('utf-8', 'backslashreplace')
        if (datos_en_string=='fin_ensayo'):
            print("saliendo de ensayo")
            break
        #re.search(r"\d+\.\d+,\d+\.\d+", datos_en_string):
        else:
            F_string,d_string = datos_en_string.split(',')
            F.append(float(F_string))
            d.append(float(d_string))
            print(F_string," - ",d_string,end="\r")
    return F, d


def guardar_datos(F, d, nombre='data'):
    nombre_completo = nombre+'.csv'
    archivo = open(nombre_completo,'w')
    archivo.write('Fuerza[kg], desplazamiento[mm]\n')
    for i in range(len(F)):
        archivo.write(str(F[i])+', '+str(d[i])+'\n')
    print('guardado como ', nombre_completo)
    return nombre_completo


def conexion(nombre_puerto=None):
    if nombre_puerto==None:
        print('Puertos seriales disponibles')
        i=0
        for puerto in serial_ports():
            i+=1
            print(i,' - ',puerto)
        n_puerto = int(input('seleccione puerto: '))

        ser=serial.Serial(serial_ports()[n_puerto-1], )
    else:
        ser=serial.Serial(nombre_puerto)
    primera_linea=ser.readline()
    print(primera_linea)
    if primera_linea==b'UniTester\r\n':
        print("Conexion establecida")
    return ser


def dibujar_curva_ed(F,d):
    plt.plot(d,F)
    plt.title('Ensayo')
    plt.xlabel('Desplazamiento [mm]')
    plt.ylabel('Fuerza [kg]')
    plt.show()

# loop de prueba



if __name__=='__main__':
    ser=conexion()
    while True:
        linea = ser.readline()
        if linea ==b'ensayando\r\n':
            F, d =capture()
            dibujar_curva_ed(F,d)
            guardado=input('Desea guardar los datos? (y/n): ')
            if guardado=='y':
                nombre_archivo = input('Nombre del archivo: ')
                if len(nombre_archivo)==0:
                    guardar_datos(F,d)
                else:
                    guardar_datos(F,d, nombre=nombre_archivo)
            elif guardado=='n':
                print('datos descartados')
            else:
                pass
            continue
        else:
            print(linea, end="\r")
