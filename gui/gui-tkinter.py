import tkinter as tk
from tkinter import filedialog
import conexion_gui
import matplotlib.pyplot as plt


class Application(tk.Frame):
    PORTS = ["0"] #etc
    F=[]
    d=[]
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.refrescar = tk.PhotoImage(file='.\\reload.png')

        self.pack()
        self.create_widgets()
        self.conexion_valida=False

    def create_widgets(self):

        self.captura_ensayo = tk.Button(self)
        self.captura_ensayo["text"] = "Capturar ensayo"
        self.captura_ensayo["command"] = self.captura
        self.captura_ensayo.pack(side = "bottom")

        self.graficar = tk.Button(self)
        self.graficar["text"] = "Graficar"
        self.graficar["command"] = self.grafica_resultados
        self.graficar.pack(side = "bottom")

        self.guardar_ensayo = tk.Button(self)
        self.guardar_ensayo["text"] = "Guardar ensayo"
        self.guardar_ensayo["command"] = self.guardar_datos
        self.guardar_ensayo.pack(side = "bottom")

        self.quit = tk.Button(self, text="Salir", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side = "bottom")

        self.tinfo = tk.Text(self.master, width=40, height=1)
        self.tinfo.pack(side = "bottom")

        #self.etiqueta1=tk.Label(self.master, text="Opciones").grid( row=0, column=0)
        #self.etiqueta2=tk.Label(self.master, text="Conexion").grid( row=0, column=1)
        self.busca_puertos = tk.Button(self)
        self.busca_puertos["text"] = "Buscar puertos"
        self.busca_puertos["command"] = self.list_serial_ports
        #self.busca_puertos.config(image=self.refrescar, width="30", height="30")
        self.busca_puertos.pack(side = "left")

        self.conectar = tk.Button(self)
        self.conectar["text"] = "Conectar"
        self.conectar["command"] = self.conecta_puerto
        self.busca_puertos.config(image=self.refrescar, width="30", height="30")
        self.conectar.pack(side = "left")

        self.variable = tk.StringVar(self.master)
        self.variable.set(self.PORTS[0]) # default value
        self.lista_puertos = tk.OptionMenu(self.master, self.variable, *self.PORTS)
        self.lista_puertos.pack(side = "left")
        
    def list_serial_ports(self):
        if len(conexion_gui.serial_ports())<1:
            self.PORTS=['0']
        else:
            self.PORTS =conexion_gui.serial_ports()
        menu = self.lista_puertos["menu"]
        menu.delete(0, "end")
        for string in self.PORTS:
            menu.add_command(label=string, 
                             command=lambda value=string: self.variable.set(value))

    
    def conecta_puerto(self):
        puerto=self.variable.get()
        self.tinfo.delete("1.0", tk.END)
        if puerto=='0':
            self.tinfo.insert("1.0", "Seleccione puerto valido")
        else:
            self.conexion_serial, self.conexion_valida =  conexion_gui.conexion(puerto)
            if self.conexion_valida:
                self.tinfo.insert("1.0", "Conectado a: "+puerto)
            else:
                self.tinfo.insert("1.0", "Dispositivo desconocido")
    
    
    def captura(self):
        if  self.conexion_valida:
            self.tinfo.delete("1.0", tk.END)
            self.tinfo.insert("1.0", "Esperando ensayo...")
            self.F,self.d = conexion_gui.espera_captura(self.conexion_serial)
            self.tinfo.delete("1.0", tk.END)
            self.tinfo.insert("1.0", "Ensayo con "+str(len(self.F))+ " puntos")
        else:
            self.tinfo.delete("1.0", tk.END)
            self.tinfo.insert("1.0", "No se ha conectado al dispositivo")
        
    

    def guardar_datos(self):
        f = filedialog.asksaveasfile(mode='w', defaultextension=".csv")
        if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        texto='Fuerza[kg], desplazamiento[mm]\n'
        for i in range(len(self.F)):
            texto+=(str(self.F[i])+', '+str(self.d[i])+'\n')
        f.write(texto)
        f.close() # `()` was missing.

    
    def grafica_resultados(self):
        plt.plot(self.d,self.F)
        plt.title('Ensayo')
        plt.xlabel('Desplazamiento [mm]')
        plt.ylabel('Fuerza [kg]')
        plt.show()

root = tk.Tk()
app = Application(master=root)
app.mainloop()