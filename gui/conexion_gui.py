import serial
import numpy as np
import re
import matplotlib.pyplot as plt
import sys
import glob


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def conexion(nombre_puerto):
    ser=serial.Serial(nombre_puerto)
    primera_linea=ser.readline()
    if primera_linea==b'UniTester\r\n':
        conexion_valida=True
    else:
        conexion_valida=False
    print("conexion: ",conexion_valida)
    return ser, conexion_valida


def loop_de_captura(ser):
    F=[]
    d=[]
    while True:
        datos_en_byte = ser.readline().strip()
        datos_en_string = datos_en_byte.decode('utf-8', 'backslashreplace')
        if (datos_en_string=='fin_ensayo'):
            break
        #re.search(r"\d+\.\d+,\d+\.\d+", datos_en_string):
        else:
            F_string,d_string = datos_en_string.split(',')
            F.append(float(F_string))
            d.append(float(d_string))
    return F, d


def espera_captura(ser):
    ser.flush()
    while True:
        linea = ser.readline()
        if linea ==b'ensayando\r\n':
            F, d =loop_de_captura(ser)
            break
        else:
            pass
    return F,d


def guardar_datos(F, d, nombre='data'):
    nombre_completo = nombre+'.csv'
    archivo = open(nombre_completo,'w')
    archivo.write('Fuerza[kg], desplazamiento[mm]\n')
    for i in range(len(F)):
        archivo.write(str(F[i])+', '+str(d[i])+'\n')
    return nombre_completo